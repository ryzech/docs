---
slug: 2021-10-07-patch-notes
title: SMP Patch Notes
authors: [ryzechdev, derpylove_11, legundo, ecr0708]
tags: [minecraft, smp, plugins]
---

The [Legundo SMP](https://legundo.mcserver.us) recently had a huge patch update! This blog post will be going over what's changed and what you should expect to see different the next time you join.
